#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch polybar
polybar bar1 &
polybar bar2 &
#if command -v xrandr >/dev/null 2>&1; then
#	for m in $(xrandr --query | grep '\<connected\>' | cut -d ' ' -f 1); do
#		MONITOR="$m" polybar --reload bar1 >/dev/null 2>&1 &
#	done
#fi
